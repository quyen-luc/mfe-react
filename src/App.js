import React from 'react'
import ReactDOM from 'react-dom/client'
import { technicalService } from 'mfe-lib'

class App extends React.Component {

  render() {
    const reactVersion = require('../package.json').dependencies['react'];
    technicalService.setTechnical('value from react');
    return ([

        <h1>
          React
          <img src="https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg" height="30"></img>
        </h1>,
        <p>
          React Version: {reactVersion} test
        </p>
    ])
  }
}

class Mfe4Element extends HTMLElement {
  connectedCallback() {
    const mountPoint = document.createElement('div');
    this.attachShadow({ mode: 'open' }).appendChild(mountPoint);
    
    const root = ReactDOM.createRoot(mountPoint);
    root.render(<App/>);
  }
}

customElements.define('react-element', Mfe4Element);